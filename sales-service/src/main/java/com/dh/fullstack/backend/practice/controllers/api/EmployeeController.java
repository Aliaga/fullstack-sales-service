package com.dh.fullstack.backend.practice.controllers.api;

import com.dh.fullstack.backend.practice.command.EmployeeCreateCmd;
import com.dh.fullstack.backend.practice.controllers.Constants;
import com.dh.fullstack.backend.practice.input.EmployeeCreateInput;
import com.dh.fullstack.backend.practice.model.domain.Employee;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author: Olegario Zelada
 */

@Api(
        tags = Constants.EmployeeTag.NAME,
        description = Constants.EmployeeTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.EMPLOYEE)
@RequestScope
public class EmployeeController {


    @Autowired
    private EmployeeCreateCmd createCmd;

    @ApiOperation(
            value = Constants.EmployeeTag.VALUE
    )
    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeCreateInput input) {

        createCmd.setInput(input);
        createCmd.execute();
        return createCmd.getEmployee();
    }
}
