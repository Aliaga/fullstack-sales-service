package com.dh.fullstack.backend.practice.model.repository;

import com.dh.fullstack.backend.practice.model.domain.Client;
import com.dh.fullstack.backend.practice.model.domain.Male;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author: Olegario Zelada
 */
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query("select item from Client item where item.male = :gender")
    List<Client> getClientDataByGender(@Param("gender") Male gender);

}
