package com.dh.fullstack.backend.practice.services;

import com.dh.fullstack.backend.practice.exception.NotFoundException;
import com.dh.fullstack.backend.practice.framework.ServiceTransactional;
import com.dh.fullstack.backend.practice.model.domain.Employee;
import com.dh.fullstack.backend.practice.model.repository.EmployeeRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author: Olegario Zelada
 */
@ServiceTransactional
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Getter
    private Employee employee;

    @Setter
    private String email;

    @Setter
    private String firstName;

    @Setter
    private String lastName;

    public void executeSearchByEmail() {
        employee = findEmployeeByEmail(email);
    }

    public void executeSearchByName() {
        employee = findEmployeeByfirstLastName(firstName, lastName);
    }

    public Employee findEmployeeByEmail(String email) {
        return employeeRepository.getEmployeeByEmailData(email)
                .orElseThrow(() -> new NotFoundException("Unable locate an employee to email= " + email));
    }

    public Employee findEmployeeByfirstLastName(String firstName, String lastName) {
        return employeeRepository.getEmployeeByFirstLastNameData(firstName, lastName)
                .orElseThrow(() -> new NotFoundException("Unable locate an employee with first name = " + firstName + " and last name = " + lastName));
    }

}
