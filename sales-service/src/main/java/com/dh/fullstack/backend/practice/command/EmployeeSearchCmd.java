package com.dh.fullstack.backend.practice.command;

import com.dh.fullstack.backend.practice.input.EmployeeSearchInput;
import com.dh.fullstack.backend.practice.model.domain.Employee;
import com.dh.fullstack.backend.practice.model.domain.Employee_;
import com.dh.fullstack.backend.practice.model.repository.EmployeeRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: Olegario Zelada
 */
@SynchronousExecution
public class EmployeeSearchCmd implements BusinessLogicCommand {

    @Getter
    List<Employee> employeeList;

    @Setter
    private EmployeeSearchInput input;

    @Setter
    private Integer limit;

    @Setter
    private Integer page;

    @Getter
    private Integer totalPages;

    @Getter
    private Long totalElements;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void execute() {

        employeeList = new ArrayList<>();

        PageRequest pageRequest = PageRequest.of(page, limit);
        Page<Employee> pageResult = employeeRepository.findAll(buildSpecification(), pageRequest);

        List<Employee> content = pageResult.getContent();
        if (!CollectionUtils.isEmpty(content)) {
            employeeList.addAll(content);
        }

        totalPages = pageResult.getTotalPages();
        totalElements = pageResult.getTotalElements();

    }

    private Specification<Employee> buildSpecification() {

        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            query.orderBy(cb.desc(root.get(Employee_.createdDate)));
            predicates.add(cb.like(root.get(Employee_.position), "%" + input.getPosition() + "%"));
            return cb.and(predicates.toArray(new Predicate[0]));
        };

    }

}
