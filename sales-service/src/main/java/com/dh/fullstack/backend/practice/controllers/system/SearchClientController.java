package com.dh.fullstack.backend.practice.controllers.system;

import com.dh.fullstack.backend.practice.controllers.Constants;
import com.dh.fullstack.backend.practice.model.domain.Client;
import com.dh.fullstack.backend.practice.model.domain.Male;
import com.dh.fullstack.backend.practice.services.ClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author: Olegario Zelada
 */

@Api(
        tags = Constants.SearchClientTag.NAME,
        description = Constants.SearchClientTag.DESCRIPTION
)
@RequestMapping(value = Constants.BasePath.SEARCH_CLIENT)
@RestController
@RequestScope
public class SearchClientController {

    @Autowired
    ClientService clientService;

    @ApiOperation(
            value = Constants.SearchClientTag.VALUE
    )
    @RequestMapping(
            value = "/{gender}",
            method = RequestMethod.GET
    )
    public List<Client> searchClientsByGender(@PathVariable("gender") Male gender) {

        clientService.setGender(gender);
        clientService.execute();

        return clientService.getClientList();

    }

}

