package com.dh.fullstack.backend.practice.model.domain;

/**
 * @author Olegario Zelada
 */
public final class Constants {

    private Constants() {
    }

    public static class PersonTable {

        public static final String NAME = "person_table";

        public static class Id {
            public static final String NAME = "personid";
        }

        public static class email {

            public static final String NAME = "email";

            public static final int LENGTH = 100;
        }

        public static class firstName {

            public static final String NAME = "firstname";

        }

        public static class lastName {

            public static final String NAME = "lastname";

        }

        public static class male {

            public static final String NAME = "male";

        }

        public static class isDeleted {

            public static final String NAME = "isdeleted";

        }

        public static class createdDate {

            public static final String NAME = "createdate";

        }

    }


    public static class ClientTable {

        public static final String NAME = "client_table";

        public static class Id {

            public static final String NAME = "clientid";

        }

        public static class PersonId {

            public static final String NAME = "personid";

        }

        public static class LastPurchase {

            public static final String NAME = "lastpurchase";

        }

    }

    public static class EmployeeTable {

        public static final String NAME = "employee_table";

        public static class Id {

            public static final String NAME = "employeeid";

        }

        public static class Position {

            public static final String NAME = "position";

        }
    }

    public static class SaleTable {

        public static final String NAME = "sale_table";

        public static class Id {

            public static final String NAME = "saleid";

        }

        public static class SaleNumber {

            public static final String NAME = "salenumber";

        }

        public static class CreateDate {

            public static final String NAME = "createdate";

        }

        public static class EmployeeSaleId {

            public static final String NAME = "employeesaleid";

        }

        public static class ClienteSaleId {

            public static final String NAME = "clientsaleid";

        }

    }

    public static class DetailTable {

        public static final String NAME = "detail_table";

        public static class Id {

            public static final String NAME = "detailid";

        }

        public static class TotalProducts {

            public static final String NAME = "totalproducts";

        }

        public static class TotalPrice {

            public static final String NAME = "totalprice";

        }

        public static class DetailSaleId {

            public static final String NAME = "detailsaleid";

        }
    }
}
