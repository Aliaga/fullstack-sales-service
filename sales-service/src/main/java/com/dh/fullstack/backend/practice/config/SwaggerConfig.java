package com.dh.fullstack.backend.practice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author: Olegario Zelada
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    private PropertiesConfig propertiesConfig;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(propertiesConfig.getGroupApiName())
                .select()
                .apis(RequestHandlerSelectors.basePackage(propertiesConfig.getBaseApiPackage()))
                .paths(PathSelectors.any())
                .build().apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);
    }

    @Bean
    public Docket system() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(propertiesConfig.getGroupSystemName())
                .select()
                .apis(RequestHandlerSelectors.basePackage(propertiesConfig.getBaseSystemPackage()))
                .paths(PathSelectors.any())
                .build().apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder()
                .title(propertiesConfig.getGroupTitle())
                .description(propertiesConfig.getGroupDescription())
                .contact(new Contact(propertiesConfig.getContactName(), "", propertiesConfig.getContactEmail()))
                .version(propertiesConfig.getVersion())

                .license(propertiesConfig.getLicense())
                .licenseUrl(propertiesConfig.getLicenseUrl())
                .build();
    }
}

