package com.dh.fullstack.backend.practice.model.repository;

import com.dh.fullstack.backend.practice.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author: Olegario Zelada
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {

    @Query("select item from Employee item where item.email = :email")
    Optional<Employee> getEmployeeByEmailData(@Param("email") String email);

    @Query("select item from Employee item where item.firstName = :firstName and item.lastName = :lastName")
    Optional<Employee> getEmployeeByFirstLastNameData(@Param("firstName") String firstName, @Param("lastName") String lastName);

}
