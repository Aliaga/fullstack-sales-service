package com.dh.fullstack.backend.practice.model.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: Olegario Zelada
 */
@Data
@Entity
@Table(name = Constants.SaleTable.NAME)
public class Sale {
    @Id
    @Column(name = Constants.SaleTable.Id.NAME, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = Constants.SaleTable.SaleNumber.NAME)
    private Long numberSale;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Constants.SaleTable.CreateDate.NAME)
    private Date createDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = Constants.SaleTable.EmployeeSaleId.NAME, referencedColumnName = Constants.EmployeeTable.Id.NAME, nullable = false)
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = Constants.SaleTable.ClienteSaleId.NAME, referencedColumnName = Constants.ClientTable.Id.NAME, nullable = false)
    private Client client;

}
