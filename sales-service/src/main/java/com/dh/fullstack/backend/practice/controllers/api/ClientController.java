package com.dh.fullstack.backend.practice.controllers.api;

import com.dh.fullstack.backend.practice.command.ClientCreateCmd;
import com.dh.fullstack.backend.practice.controllers.Constants;
import com.dh.fullstack.backend.practice.input.ClientCreateInput;
import com.dh.fullstack.backend.practice.model.domain.Client;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author: Olegario Zelada
 */

@Api(
        tags = Constants.ClientTag.NAME,
        description = Constants.ClientTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.CLIENT)
@RequestScope
public class ClientController {


    @Autowired
    private ClientCreateCmd createCmd;

    @ApiOperation(
            value = Constants.ClientTag.VALUE
    )
    @RequestMapping(method = RequestMethod.POST)
    public Client createEmployee(@RequestBody ClientCreateInput input) {

        createCmd.setInput(input);
        createCmd.execute();
        return createCmd.getClient();
    }
}
