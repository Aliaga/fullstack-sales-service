package com.dh.fullstack.backend.practice.command;

import com.dh.fullstack.backend.practice.input.ClientCreateInput;
import com.dh.fullstack.backend.practice.model.domain.Client;
import com.dh.fullstack.backend.practice.model.repository.ClientRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author: Olegario Zelada
 */
@SynchronousExecution
public class ClientCreateCmd implements BusinessLogicCommand {

    @Getter
    private Client client;

    @Setter
    private ClientCreateInput input;

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public void execute() {

        client = clientRepository.save(composeClientInstance());
    }

    private Client composeClientInstance() {

        Client instance = new Client();
        instance.setEmail(input.getEmail());
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());
        instance.setLastPurchase(new Date());
        instance.setMale(input.getMale());

        return instance;
    }

}
