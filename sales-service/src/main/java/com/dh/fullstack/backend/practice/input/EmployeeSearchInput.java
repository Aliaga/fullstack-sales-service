package com.dh.fullstack.backend.practice.input;

import lombok.Getter;
import lombok.Setter;

/**
 * @author: Olegario Zelada
 */
public class EmployeeSearchInput {

    @Getter
    @Setter
    private String position;

}
