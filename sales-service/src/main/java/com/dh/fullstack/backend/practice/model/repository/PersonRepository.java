package com.dh.fullstack.backend.practice.model.repository;

import com.dh.fullstack.backend.practice.model.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author: Olegario Zelada
 */
public interface PersonRepository extends JpaRepository<Person, Long> {
}
