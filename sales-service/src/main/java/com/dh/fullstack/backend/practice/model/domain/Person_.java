package com.dh.fullstack.backend.practice.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author: Olegario Zelada
 */


@StaticMetamodel(Person.class)
public class Person_ {

    public static volatile SingularAttribute<Person, Long> id;

    public static volatile SingularAttribute<Person, String> email;

    public static volatile SingularAttribute<Person, String> firstName;

    public static volatile SingularAttribute<Person, Male> male;

    public static volatile SingularAttribute<Person, Boolean> isDeleted;

    public static volatile SingularAttribute<Person, Date> createdDate;

}
