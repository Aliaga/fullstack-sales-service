package com.dh.fullstack.backend.practice.input;

import com.dh.fullstack.backend.practice.model.domain.Male;
import lombok.Data;

import java.util.Date;

/**
 * @author: Olegario Zelada
 */
@Data
public class ClientCreateInput {

    private String email;

    private String firstName;

    private String lastName;

    private Date lastPurchase;

    private Male male;

}
