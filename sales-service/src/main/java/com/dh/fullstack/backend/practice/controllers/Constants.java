package com.dh.fullstack.backend.practice.controllers;

/**
 * @author Olegario Zelada
 */
public final class Constants {

    private Constants() {
    }

    public static class EmployeeTag {

        public static final String NAME = "employee-controller";

        public static final String DESCRIPTION = "Available operations over employee";

        public static final String VALUE = "End point to create an employee";
    }

    public static class ClientTag {

        public static final String NAME = "client-controller";

        public static final String DESCRIPTION = "Available operations over client";

        public static final String VALUE = "End point to create a client";

    }

    public static class BasePath {

        public static final String PUBLIC = "/public";

        public static final String SYSTEM = "/system";

        public static final String EMPLOYEE = PUBLIC + "/employee";

        public static final String CLIENT = PUBLIC + "/client";

        public static final String SEARCH_EMPLOYEE = SYSTEM + "/employee";

        public static final String SEARCH_CLIENT = SYSTEM + "/client";

    }

    public static class SearchEmployeeTag {

        public static final String NAME = "search-employee-controller";

        public static final String DESCRIPTION = "Available search operation over employee";

        public static final String VALUE_EMAIL = "End point to search an employee by E-mail";

        public static final String VALUE_NAME = "End point to search an employee by First and Last name";

        public static final String VALUE_LIST = "End point to search all employee List by position";

    }

    public static class SearchClientTag {

        public static final String NAME = "search-client-controller";

        public static final String DESCRIPTION = "Available search operation over client";

        public static final String VALUE = "End point to search clients by gender";

    }

}
