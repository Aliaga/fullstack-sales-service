package com.dh.fullstack.backend.practice.services;

import com.dh.fullstack.backend.practice.framework.ServiceTransactional;
import com.dh.fullstack.backend.practice.model.domain.Client;
import com.dh.fullstack.backend.practice.model.domain.Male;
import com.dh.fullstack.backend.practice.model.repository.ClientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author: Olegario Zelada
 */
@ServiceTransactional
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Getter
    private List<Client> clientList;

    @Setter
    private Male gender;

    public void execute() {

        clientList = findAllClientsByGender(gender);
    }

    public List<Client> findAllClientsByGender(Male gender) {

        return clientRepository.getClientDataByGender(gender);

    }

}
