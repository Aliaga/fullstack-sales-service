package com.dh.fullstack.backend.practice.model.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * @author: Olegario Zelada
 */
@Data
@Entity
@Table(name = Constants.DetailTable.NAME)
public class Detail {

    @Id
    @Column(name = Constants.DetailTable.Id.NAME, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = Constants.DetailTable.TotalProducts.NAME)
    private Integer totalProducts;

    @Column(name = Constants.DetailTable.TotalPrice.NAME)
    private Long totalPrice;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = Constants.DetailTable.DetailSaleId.NAME, referencedColumnName = Constants.SaleTable.Id.NAME, nullable = false)
    private Sale sale;

}
