package com.dh.fullstack.backend.practice.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author: Olegario Zelada
 */
@Configuration
@PropertySource("classpath:/configuration/api-version.properties")
public class PropertiesConfig {

    @Getter
    @Value("${application.swagger.baseApiPackage}")
    private String baseApiPackage;

    @Getter
    @Value("${application.swagger.baseSystemPackage}")
    private String baseSystemPackage;

    @Getter
    @Value("${application.swagger.groupApiName}")
    private String groupApiName;

    @Getter
    @Value("${application.swagger.groupSystemName}")
    private String groupSystemName;

    @Getter
    @Value("${application.swagger.groupTitle}")
    private String groupTitle;

    @Getter
    @Value("${application.swagger.groupDescription}")
    private String groupDescription;

    @Getter
    @Value("${application.swagger.contactName}")
    private String contactName;

    @Getter
    @Value("${application.swagger.contactEmail}")
    private String contactEmail;

    @Getter
    @Value("${application.swagger.version}")
    private String version;

    @Getter
    @Value("${application.swagger.license}")
    private String license;

    @Getter
    @Value("${application.swagger.licenseUrl}")
    private String licenseUrl;

}
