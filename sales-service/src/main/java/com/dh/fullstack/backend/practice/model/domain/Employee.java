package com.dh.fullstack.backend.practice.model.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author: Olegario Zelada
 */
@Data
@Entity
@Table(name = Constants.EmployeeTable.NAME)
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = Constants.EmployeeTable.Id.NAME, referencedColumnName = Constants.PersonTable.Id.NAME)
})
public class Employee extends Person implements Serializable {

    @Column(name = Constants.EmployeeTable.Position.NAME)
    private String position;

}
