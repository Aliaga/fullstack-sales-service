package com.dh.fullstack.backend.practice.model.repository;

import com.dh.fullstack.backend.practice.model.domain.Detail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author: Olegario Zelada
 */
public interface DetailRepository extends JpaRepository<Detail, Long> {
}
