package com.dh.fullstack.backend.practice;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dh.fullstack.backend.practice")
@EnableAutoConfiguration
public class Config {

}
