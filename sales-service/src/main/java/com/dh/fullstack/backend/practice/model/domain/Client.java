package com.dh.fullstack.backend.practice.model.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: Olegario Zelada
 */
@Data
@Entity
@Table(name = Constants.ClientTable.NAME)
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = Constants.ClientTable.Id.NAME, referencedColumnName = Constants.ClientTable.PersonId.NAME)
})
public class Client extends Person {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Constants.ClientTable.LastPurchase.NAME, nullable = false, updatable = false)
    private Date lastPurchase;

}
