package com.dh.fullstack.backend.practice.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author: Olegario Zelada
 */
@StaticMetamodel(Sale.class)
public class Sale_ {

    public static volatile SingularAttribute<Sale, Long> id;

    public static volatile SingularAttribute<Sale, Long> numberSale;

    public static volatile SingularAttribute<Sale, Date> createDate;

    public static volatile SingularAttribute<Sale, Employee> employee;

    public static volatile SingularAttribute<Sale, Client> client;

}
