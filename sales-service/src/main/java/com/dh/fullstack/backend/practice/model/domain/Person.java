package com.dh.fullstack.backend.practice.model.domain;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: Olegario Zelada
 */
@Data
@Entity
@Table(name = Constants.PersonTable.NAME)
@Inheritance(strategy = InheritanceType.JOINED)
public class Person {

    @Id
    @Column(name = Constants.PersonTable.Id.NAME, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = Constants.PersonTable.email.NAME, length = Constants.PersonTable.email.LENGTH)
    private String email;

    @Column(name = Constants.PersonTable.firstName.NAME)
    private String firstName;

    @Column(name = Constants.PersonTable.lastName.NAME)
    private String lastName;

    @Column(name = Constants.PersonTable.male.NAME)
    private Male male;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = Constants.PersonTable.isDeleted.NAME, nullable = false)
    private Boolean isDeleted;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Constants.PersonTable.createdDate.NAME, nullable = false, updatable = false)
    private Date createdDate;

    @PrePersist
    void onPrePersist() {
        this.createdDate = new Date();
        this.isDeleted = Boolean.FALSE;
    }

}
