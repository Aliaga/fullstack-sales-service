package com.dh.fullstack.backend.practice.model.repository;

import com.dh.fullstack.backend.practice.model.domain.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author: Olegario Zelada
 */
public interface SaleRepository extends JpaRepository<Sale, Long> {
}
