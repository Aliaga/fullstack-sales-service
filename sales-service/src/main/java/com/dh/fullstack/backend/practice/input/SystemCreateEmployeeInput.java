package com.dh.fullstack.backend.practice.input;

import com.dh.fullstack.backend.practice.model.domain.Male;
import lombok.Data;

/**
 * @author Olegario Zelada
 */
@Data
public class SystemCreateEmployeeInput {

    private String email;

    private String firstName;

    private String lastName;

    private String position;

    private Male male;

    private String contactName;

    private String contactEmail;

}
