package com.dh.fullstack.backend.practice.command;

import com.dh.fullstack.backend.practice.input.EmployeeCreateInput;
import com.dh.fullstack.backend.practice.model.domain.Employee;
import com.dh.fullstack.backend.practice.model.repository.EmployeeRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author: Olegario Zelada
 */
@SynchronousExecution
public class EmployeeCreateCmd implements BusinessLogicCommand {

    @Getter
    private Employee employee;

    @Setter
    private EmployeeCreateInput input;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void execute() {

        employee = employeeRepository.save(composeEmployeeInstance());
    }

    private Employee composeEmployeeInstance() {

        Employee instance = new Employee();
        instance.setPosition(input.getPosition());
        instance.setEmail(input.getEmail());
        instance.setMale(input.getMale());
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());

        return instance;
    }

}
