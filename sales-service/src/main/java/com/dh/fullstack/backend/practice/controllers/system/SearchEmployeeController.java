package com.dh.fullstack.backend.practice.controllers.system;

import com.dh.fullstack.backend.practice.command.EmployeeSearchCmd;
import com.dh.fullstack.backend.practice.commons.Pagination;
import com.dh.fullstack.backend.practice.controllers.Constants;
import com.dh.fullstack.backend.practice.input.EmployeeSearchInput;
import com.dh.fullstack.backend.practice.model.domain.Employee;
import com.dh.fullstack.backend.practice.services.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author: Olegario Zelada
 */

@Api(
        tags = Constants.SearchEmployeeTag.NAME,
        description = Constants.SearchEmployeeTag.DESCRIPTION
)
@RequestMapping(value = Constants.BasePath.SEARCH_EMPLOYEE)
@RestController
@RequestScope
public class SearchEmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeSearchCmd searchCmd;

    @ApiOperation(
            value = Constants.SearchEmployeeTag.VALUE_EMAIL
    )
    @RequestMapping(
            value = "/{email}",
            method = RequestMethod.GET
    )
    public Employee searchEmployeeByEmail(@PathVariable("email") String email) {

        employeeService.setEmail(email);
        employeeService.executeSearchByEmail();

        return employeeService.getEmployee();
    }

    @ApiOperation(
            value = Constants.SearchEmployeeTag.VALUE_NAME
    )
    @RequestMapping(
            value = "/{firstName}/{lastName}",
            method = RequestMethod.GET
    )
    public Employee searchEmployeeByName(@PathVariable("firstName") String firstName, @PathVariable("lastName") String lastName) {

        employeeService.setFirstName(firstName);
        employeeService.setLastName(lastName);
        employeeService.executeSearchByName();

        return employeeService.getEmployee();
    }


    @ApiOperation(
            value = Constants.SearchEmployeeTag.VALUE_LIST
    )
    @RequestMapping(
            value = "/search",
            method = RequestMethod.POST)
    public Pagination<Employee> searchContact(@RequestParam("limit") Integer limit,
                                              @RequestParam("page") Integer page,
                                              @RequestBody EmployeeSearchInput input) {

        searchCmd.setLimit(limit);
        searchCmd.setPage(page);
        searchCmd.setInput(input);
        searchCmd.execute();

        Pagination<Employee> pagination = new Pagination<>();
        pagination.setContent(searchCmd.getEmployeeList());
        pagination.setTotalPages(searchCmd.getTotalPages());
        pagination.setTotalElements(searchCmd.getTotalElements());

        return pagination;
    }

}

